# Apidoc OpenPatch Template

![Screenshot](screenshot.png)

## Improvements

* When no Content-Type is set, `application/json` is assumed
* Errors will be listed in a compact format. Therefore every `@apiError` needs to be formated like this:

```
@apiError (ErrorCode xxx) {Integer} code A custom error code xxx
@apiError (ErrorCode xxx) {String} message A message explaining the error
@apiError (ErrorCode xxx) {Integer} status_code The http response code
```

Example:

```
@apiError (ErrorCode 100) {Integer} code 100
@apiError (ErrorCode 100) {String} message Something went wrong 
@apiError (ErrorCode 100) {Integer} status_code 400 
```

## Usage

Clone this repository to any directory and use it with apidoc's `template` option.

```shell
git clone https://gitlab.com/openpatch/apidoc-template.git
cd yourproject
apidoc -t ../apidoc-template
```
